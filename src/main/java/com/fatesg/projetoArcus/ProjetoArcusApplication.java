package com.fatesg.projetoArcus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoArcusApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoArcusApplication.class, args);
	}

}
